function ListOfPeople(){
    var people =[];
    var self = this;
    self.addPerson = function(json){
        people.push(new Person(json));
    }
    self.toTable = function(){
        var table = '<table>';
        table += generateTableHeader();

        for(var i =0;i<people.length;i++) {
            table+=people[i].toTableRow(i);
        }
        table+='</table>'
        return table;
    }
    self.clear = function(){
        people = [];
    }
    var generateTableHeader= function(){
        if(viewModel.hide())
        {
        return '<tr><th>Licznik</th>'
            +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byId)} else viewModel.sort(comparator.byIdDesc)">Id</button></th>'
            +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byName)} else viewModel.sort(comparator.byNameDesc)">Name</button></th>'
            +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.bySurname)} else viewModel.sort(comparator.bySurnameDesc)">Surname</button></th>'
            +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byGender)} else viewModel.sort(comparator.byGenderDesc)">Gender</button></th>'
            +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byEmail)} else viewModel.sort(comparator.byEmailDesc)">Email</button></th>'
            +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byAge)} else viewModel.sort(comparator.byAgeDesc)">Age</button></th>'
            +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byBirthday)} else viewModel.sort(comparator.byBirthdayDesc)">Birthday</button></th>'
            +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byIncome)} else viewModel.sort(comparator.byIncomeDesc)">Income</button></th></tr>'
        }
        else
        {
            return '<tr><th>Licznik</th>'
                +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byId)} else viewModel.sort(comparator.byIdDesc)">Id</button></th>'
                +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byName)} else viewModel.sort(comparator.byNameDesc)">Name</button></th>'
                +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.bySurname)} else viewModel.sort(comparator.bySurnameDesc)">Surname</button></th>'
                +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byGender)} else viewModel.sort(comparator.byGenderDesc)">Gender</button></th>'
                +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byAge)} else viewModel.sort(comparator.byAgeDesc)">Age</button></th>'
                +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byBirthday)} else viewModel.sort(comparator.byBirthdayDesc)">Birthday</button></th>'
                +'<th><button onclick="if(viewModel.change()){viewModel.sort(comparator.byIncome)} else viewModel.sort(comparator.byIncomeDesc)">Income</button></th></tr>'
        }
    }
}
