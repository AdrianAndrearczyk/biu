function Comparators() {
  var self = this;

  //name
  self.byName = function(person1, person2){
    return person1.firstName.localeCompare(person2.firstName);
  };
  self.byNameDesc = function(person2, person1){
    return person1.firstName.localeCompare(person2.firstName);
  };

  //surname
  self.bySurname = function(person1, person2){
    return person1.lastName.localeCompare(person2.lastName);
  };
  self.bySurnameDesc = function(person2, person1){
    return person1.lastName.localeCompare(person2.lastName);
  };

  //id
  self.byId = function(person1, person2){
    return person1.id - person2.id;
  };
  self.byIdDesc = function(person2, person1){
    return person1.id - person2.id;
  };

  //gender
  self.byGender = function(person1, person2) {
    return person1.gender.localeCompare(person2.gender);
  };
  self.byGenderDesc = function(person2, person1) {
    return person1.gender.localeCompare(person2.gender);
  };

  //age
  self.byAge = function(person1, person2) {
    return person1.age - person2.age;
  };
  self.byAgeDesc = function(person2, person1) {
    return person1.age - person2.age;
  };

  //income
  self.byIncome = function(person1, person2){
    return person1.income - person2.income
  };
  self.byIncomeDesc = function(person2, person1){
    return person1.income - person2.income
  };

  //birthday
  self.byBirthday = function(person1, person2){
    return person1.birthsday.localeCompare(person2.birthsday);
  };
  self.byBirthdayDesc = function(person2, person1){
    return person1.birthsday.localeCompare(person2.birthsday);
  };

  //email
  self.byEmail = function(person1 ,person2) {
    return person1.email.localeCompare(person2.email);
  };
  self.byEmailDesc = function(person2 ,person1) {
    return person1.email.localeCompare(person2.email);
  };
}
