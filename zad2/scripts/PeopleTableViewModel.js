function PeopleTableViewModel(config){
    var self = this;
    self.people = new ListOfPeople();
    self.currentPage = 0;
    self.pageSize = config.pageSize;
    self.context = config.context;
    var ascending = true;
    var hider = true;
    var counter = 0;

    self.next = function() {
        self.people.clear();
        var begin = (self.currentPage) * self.pageSize;
        var end = (self.currentPage + 1) * self.pageSize;
        getData(begin, end);
            self.currentPage++;
        self.context.innerHTML=self.people.toTable();
    }

    self.prev = function(){
        self.people.clear();
        if(self.currentPage>=0)
        {
            self.currentPage--;
        }
        var begin = (self.currentPage-1)*self.pageSize;
        var end = (self.currentPage)*self.pageSize;
        getData(begin,end);
        self.context.innerHTML=self.people.toTable();
    }

    self.dwa = function() {
      self.pageSize = 25;
      self.currentPage = 0;
      self.next();
    }

    self.piec = function() {
      self.pageSize = 50;
      self.currentPage = 0;
      self.next();
    }

    self.siedem = function() {
      self.pageSize = 75;
      self.currentPage = 0;
      self.next();
    }

    self.sto = function() {
      self.pageSize = 100;
      self.currentPage = 0;
      self.next();
    }


    var getData = function(begin, end){
        if(end>data.length){
            end=data.length
        }
        if(begin<0) {
            begin =0;
        }
        for(var i = begin; i<end;i+=1){
            self.people.addPerson(data[i]);
        }
    }

    self.sort = function(comparer){
        data.sort(comparer);
        self.currentPage = 0;
        self.next();
    }

    self.change = function()
    {
    if(ascending==true)
    {
        ascending = false;
    }
    else
    {
        ascending = true;
    }
    return ascending;
    }

    self.showHide = function()
    {
        if(hider==true)
        {
            hider = false;
        }
        else
        {
            hider = true;
        }
        self.people.clear();
        var begin = (self.currentPage-1)*self.pageSize;
        var end =(self.currentPage)*self.pageSize;
        getData(begin,end);
        self.context.innerHTML=self.people.toTable();
    }

    self.hide = function()
    {
        return hider;
    }



    self.count = function()
    {
        if(self.currentPage-1>=0)
        {
            counter = (self.currentPage-1) * self.pageSize;
        }
        return counter;
    }
}