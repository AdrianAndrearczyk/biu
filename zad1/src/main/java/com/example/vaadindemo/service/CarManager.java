package com.example.vaadindemo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.example.vaadindemo.domain.Car;

public class CarManager {
	
	private List<Car> db = new ArrayList<Car>();
	private int table[] = new int[10];
	
	public List<String> giveBrandNames()
	{
		List<String> brandNames = new ArrayList<String>();
		brandNames.add("Nissan");
		brandNames.add("Daewoo");
		brandNames.add("Skoda");
		brandNames.add("Fiat");
		brandNames.add("BMW");
		brandNames.add("Mercedes");
		brandNames.add("Audi");
		brandNames.add("Peugeot");
		brandNames.add("Opel");
		brandNames.add("Toyota");
		return brandNames;
	}
	
	public void addPerson(Car car){
		Car c = new Car(car.getBrand(), car.getModel(), car.getYop());
		c.setId(UUID.randomUUID());
		db.add(c);
	}
	
	public List<Car> findAll(){
		return db;
	}

	public void delete(Car car) {
		
		Car toRemove = null;
		for (Car c: db) {
			if (c.getId().equals(car.getId())){
				toRemove = c;
				break;
			}
		}
		db.remove(toRemove);		
	}
	
	public List<Integer> giveNumbers() //Lista z ilością marek samochodów
	{
		for(int i=0; i<table.length; i++){
			table[i]=0;
		}
		List<Integer> numbers = new ArrayList<Integer>();
		for(Car c: db)
		{
			for(int i = 0; i < 10; i++)
			{
				if(c.getBrand().equals(giveBrandNames().get(i)))
					table[i]++;
			}
			
		}	
		for(int i = 0; i < table.length; i++)
		{
		numbers.add(table[i]);
		}
		
		return numbers;
	}

}

