package com.example.vaadindemo.domain;

import java.util.UUID;

public class Car {
	
	private UUID id;
	private String brand;
	private String model;
	private int yop; //year of production
	
	public Car(String brand, String model, int yop) {
		super();
		this.brand = brand;
		this.model = model;
		this.yop = yop;
	}

	public Car() {

	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getYop() {
		return yop;
	}

	public void setYop(int yop) {
		this.yop = yop;
	}

	@Override
	public String toString() {
		return "Car [brand=" + brand + ", yop=" + yop
				+ ", model=" + model + "]";
	}

}
