	package com.example.vaadindemo;


import com.example.vaadindemo.domain.Car;
import com.example.vaadindemo.service.CarManager;
import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;
import com.vaadin.addon.charts.model.PlotOptionsPie;
import com.vaadin.annotations.Title;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;


@Title("Projekt Vaadin")
public class VaadinApp extends UI {
	
	class yopValidator implements Validator {
	    @Override
	    public void validate(Object value)
	            throws InvalidValueException {
	        if (!((Integer)value <= 2016 && ((Integer)value >= 1900)))
	            throw new InvalidValueException("Wprowadź datę od 1900 do 2016");
	    }
	}
	private static final long serialVersionUID = 1L;

	private CarManager carManager = new CarManager();

	private Car car = new Car("Nissan", "Micra", 2010);
	private BeanItem<Car> carItem = new BeanItem<Car>(car);

	private BeanItemContainer<Car> cars = new BeanItemContainer<Car>(
			Car.class);

	enum Action {
		ADD;
	}
	
	//Regex
	String carModelRegexp = "[A-Z][a-zA-Z]*";


	//Walidator roku produkcji
	private class MyFormWindow extends Window {
		private static final long serialVersionUID = 1L;
		
		
		
		
		private Action action;

		public MyFormWindow(Action act) {
			this.action = act;

			setModal(true);
			center();
			
			switch (action) {
			case ADD:
				setCaption("Dodaj nowy samochód");
				break;
			default:
				break;
			}
			
			final FormLayout form = new FormLayout();
			final FieldGroup binder = new FieldGroup(carItem);
			
			final NativeSelect select = new NativeSelect("Marka");
			for(int i = 0; i < carManager.giveBrandNames().size(); i++)
			{
				select.addItem(carManager.giveBrandNames().get(i));
			}
			select.setNullSelectionAllowed(false);
			
			final Button saveBtn = new Button(" Dodaj samochód ");
			final Button cancelBtn = new Button(" Anuluj ");

			form.addComponent(select);
			//form.addComponent(binder.buildAndBind("Marka", "brand"));
			form.addComponent(binder.buildAndBind("Model", "model"));
			form.addComponent(binder.buildAndBind("Rok produkcji", "yop"));

			binder.setBuffered(true);

			//binder.getField("brand").setRequired(true);
			
		
			binder.getField("model").setRequired(true);
			binder.getField("model").addValidator(new RegexpValidator(carModelRegexp, "Podaj nazwę z wielkiej litery"));
			binder.getField("yop").setRequired(true);
			binder.getField("yop").addValidator(new yopValidator());


			VerticalLayout fvl = new VerticalLayout();
			fvl.setMargin(true);
			fvl.addComponent(form);

			HorizontalLayout hl = new HorizontalLayout();
			hl.addComponent(saveBtn);
			hl.addComponent(cancelBtn);
			fvl.addComponent(hl);

			setContent(fvl);

			saveBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						binder.commit();

						if (action == Action.ADD) {
							car.setBrand(select.getValue().toString());
							carManager.addPerson(car);
						} 

						cars.removeAllItems();
						cars.addAll(carManager.findAll());
						close();
					} catch (CommitException e) {
						e.printStackTrace();
					}
				}
			});

			cancelBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					binder.discard();
					close();
				}
			});
		}
	}

	@Override
	protected void init(VaadinRequest request) {

		Button addCarFormBtn = new Button("Add ");
		Button deleteCarFormBtn = new Button("Delete");
		Button showCarChartBtn = new Button(" Show chart");

		VerticalLayout vl = new VerticalLayout();
		setContent(vl);
		
		
		//Dodawanie samochodu
		addCarFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				addWindow(new MyFormWindow(Action.ADD));
			}
		});

		
		//Usuwanie samochodu
		deleteCarFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (!car.getBrand().isEmpty()) {
					carManager.delete(car);
					cars.removeAllItems();
					cars.addAll(carManager.findAll());
				}
			}
		});
		
		//Wykres kołowy
		Chart chart = new Chart(ChartType.PIE);
		Configuration conf = chart.getConfiguration();
		DataSeries series = new DataSeries();

		
		PlotOptionsPie options = new PlotOptionsPie();
		
		chart.setWidth("400px");  // 100% by default
		chart.setHeight("300px"); // 400px by default
		chart.drawChart();
		conf.setTitle("Marki samochodów");

		options.setInnerSize("0");
		options.setSize("75%");  // Default
		options.setCenter("50%", "50%"); // Default
		conf.setPlotOptions(options);
		chart.setImmediate(isImmediate());
		conf.addSeries(series); //dodanie danych do konfiguracji wykresu
		
		showCarChartBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			
			@Override
			public void buttonClick(ClickEvent event) {

				series.clear();

				for(int i = 0; i<carManager.giveNumbers().size(); i++)
				{
					if(carManager.giveNumbers().get(i) != 0)
					{
						series.add(new DataSeriesItem(carManager.giveBrandNames().get(i), carManager.giveNumbers().get(i)));
					}
				}

				chart.drawChart(); //Rysowanie wykresu
				
			}
		}
				);

		
		
		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(addCarFormBtn);
		hl.addComponent(deleteCarFormBtn);
		hl.addComponent(showCarChartBtn);

		TextField brandEditor = new TextField();
		TextField modelEditor = new TextField();
		TextField yopEditor = new TextField();

		brandEditor.addValidator(new RegexpValidator(
			    "[A-Z][a-zA-Z]*",
			    "Podaj nazwę marki z wielkiej litery"));
		
		modelEditor.addValidator(new RegexpValidator(
			    "[A-Z][a-zA-Z]*",
			    "Podaj nazwę modleu z wielkiej litery"));
		
		yopEditor.addValidator(new yopValidator());
		
		//Grid
		final Grid grid = new Grid(cars);
		grid.setColumnOrder("brand", "model", "yop");
		grid.removeColumn("id");
		grid.getColumn("brand").setHeaderCaption("Marka").setEditorField(brandEditor);
		grid.getColumn("model").setHeaderCaption("Model").setEditorField(modelEditor);
		grid.getColumn("yop").setHeaderCaption("Rok produkcji").setEditorField(yopEditor);
		grid.setEditorEnabled(true);
		
		
		grid.getColumn("brand").setExpandRatio(1).setMaximumWidth(175).setMinimumWidth(175);
		grid.getColumn("model").setExpandRatio(1).setMaximumWidth(175).setMinimumWidth(175);
		grid.getColumn("yop").setExpandRatio(1).setMaximumWidth(150).setMinimumWidth(150);
		grid.setFrozenColumnCount(3);
		
		deleteCarFormBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (!car.getBrand().isEmpty()) {
					carManager.delete(car);
					cars.removeAllItems();
					cars.addAll(carManager.findAll());
				}
			}
		});
			
		//Grid - wybieranie linii do usunięcia
		grid.addSelectionListener(selectionEvent -> {
		    Object selected = ((SingleSelectionModel)
		        grid.getSelectionModel()).getSelectedRow();
		    Car selectedCar = (Car) grid.getSelectedRow();
		    if (selected != null){
		        Notification.show("Wybrano " + selectedCar.getBrand() + " " + selectedCar.getModel());
		    car.setBrand(selectedCar.getBrand());
			car.setModel(selectedCar.getModel());
			car.setYop(selectedCar.getYop());
			car.setId(selectedCar.getId());
		    }
		    else {
				Notification.show("Nic nie wybrano");
			}
		});
		
		
		vl.addComponent(hl);
		vl.addComponent(grid);
		vl.addComponent(chart);	
		
	}

}
