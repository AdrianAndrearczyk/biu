import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public contact = {
    firstName: "John",
    lastName:"Smith",
    phoneNumber:"123 456 789",
    email:"mail@yourwebsite.com",
    website:"www.yourwebsite.com",
    position:"WebDeveloper",
    company:"Your Company Name"
  };

}
