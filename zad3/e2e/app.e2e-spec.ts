import { Zad3Page } from './app.po';

describe('zad3 App', function() {
  let page: Zad3Page;

  beforeEach(() => {
    page = new Zad3Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
