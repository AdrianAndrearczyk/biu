import { Component, Input} from '@angular/core';
import {Book} from "../book";

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements Input{

  @Input()
  book: Book;

  add():void
  {
    if(this.book.voted==false)
    {
    this.book.likes+=1;
    }
    this.book.voted=true;
  }
  substract():void
  {
    if(this.book.likes>0)
    {
      if(this.book.voted==false)
      {
        this.book.likes-=1;
      }
      this.book.voted=true;
    }
  }

}
