import {Pipe, PipeTransform} from '@angular/core';
import {Book} from "./book";

@Pipe({
  name: 'sortBy',
  pure: false
})


export class SortBy implements PipeTransform {
  transform(array: Array<Book>, args: string): Array<Book> {
    array.sort((first: any, second: any) => {
      if (first.likes > second.likes)
      {
        return -1;
      }
      else if (first.likes < second.likes)
      {
        return 1;
      } else
      {
        return 0;
      }
    });
    return array;
  }
}
