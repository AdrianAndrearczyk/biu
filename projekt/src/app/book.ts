export class Book {
  id: number;
  title: string;
  author: string;
  isbn: string;
  publisher?: string;
  likes: number;
  voted: boolean;
}
