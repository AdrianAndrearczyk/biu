import {Component, OnInit, Injectable} from '@angular/core';
import {FormBuilder, Validators, AbstractControl} from '@angular/forms';
import {FormGroup, FormControl } from '@angular/forms';
import {Book} from "../book";
import {BooksComponent} from "../books/books.component"




@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css'],
  providers: [BooksComponent]
})

@Injectable()
export class AddbookComponent implements OnInit {
  book: Book;
  myForm: FormGroup;
  selectedBook: Book;
  title: AbstractControl;
  author: AbstractControl;
  isbn: AbstractControl;
  publisher: AbstractControl;

  publishers = [
  {id: 1, name:'Helion'},
  {id: 2, name:'Muza'},
  {id: 3, name:'Agawa'},
  {id: 4, name:'Egmont'},
  {id: 5, name:'Nowa Era'},
  {id: 6, name:'Operon'},
  {id: 7, name: 'Punkt'}

  ];

  constructor(fb: FormBuilder, private booksList: BooksComponent) {

    this.myForm = fb.group({
      'title' : ['',Validators.compose([Validators.required, this.malaLiteraValidator])],
      'author' : ['',Validators.compose([Validators.required, this.malaLiteraValidator])],
      'isbn' : ['', Validators.compose([Validators.required, Validators.minLength(10), this.isbnValidator])],
      'publisher' : ['',Validators.required]})

    this.title = this.myForm.controls['title'];
    this.author = this.myForm.controls['author'];
    this.isbn = this.myForm.controls['isbn'];
    this.publisher = this.myForm.controls['publisher'];
  }
  books: Book[];

  getBooks()
  {
    this.books = this.booksList.getBooks();
  }

  ngOnInit(): void
  {
    this.getBooks();
  }

  bookSend(form : any)
  {
    this.book = {
      id: this.books.length + 1,
      title: form.title,
      author: form.author,
      isbn: form.isbn.replace("-", ""),
      publisher: form.publisher,
      likes: Math.floor((Math.random() * 10) + 1),
      voted: false
    }
    this.books.push(this.book);
  }

  onSelect(book: Book): void
  {
    this.selectedBook = book;
  }

  malaLiteraValidator(control: FormControl):{[s: string]: boolean;}
  {
   //   if(!control.value.match(/^[A-Z][a-z0-9_-]/))
      if(control.value.charAt(0) != control.value.charAt(0).toUpperCase())
      {
        return {'lowerCase': true};
      }
    }

  isbnValidator(control: FormControl):{[s: string]: boolean;}
  {
    var number = control.value;
    number = number.replace(/[^\dX]/gi, '');
      if(number.length == 10) {
        var chars = number.split('');
        if(chars[9].toUpperCase() == 'X') {
          chars[9] = 10;
        }
        var sum = 0;
        for(var i = 0; i < chars.length; i++) {
          sum += ((10-i) * parseInt(chars[i]));
        }
        if (!(sum % 11 == 0)) return {'isbnValidate': true};
      } else if(number.length == 13) {
        var chars = number.split('');
        var sum = 0;
        for (var i = 0; i < chars.length; i++) {
          if(i % 2 == 0) {
            sum += parseInt(chars[i]);
          } else {
            sum += parseInt(chars[i]) * 3;
          }
        }
        if (!(sum % 10 == 0)) return {'isbnValidate': true};
      } else {
        return {'isbnValidate': true};
      }
    }

}
