import {Component, Injectable} from '@angular/core';
import {Book} from "../book";

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})

@Injectable()
export class BooksComponent{
  selectedBook : Book;
  books: Book[] = [
    { id: 1, title: 'Hobbit', author: 'Tolkien', isbn: '978-0547928227', publisher: 'Amber', likes: Math.floor((Math.random() * 10) + 1), voted: false},
    { id: 2, title: 'Władca pierścieni', author: 'Tolkien', isbn: '978-0547928333', publisher: 'Muza SA', likes: Math.floor((Math.random() * 10) + 1), voted: false},
    { id: 3, title: 'Krzyżacy', author: 'Henryk Sienkiewicz', isbn: '978-8378873976', publisher: 'Dragon', likes: Math.floor((Math.random() * 10) + 1), voted: false},
    { id: 4, title: 'W pustyni i w puszczy', author: 'Henryk Sienkiewicz', isbn: '978-8388714009', publisher: 'Greg', likes: Math.floor((Math.random() * 10) + 1), voted: false},
    { id: 5, title: 'Czysty kod', author: 'Robert C. Martin', isbn: '978-8328302341', publisher: 'Helion', likes: Math.floor((Math.random() * 10) + 1), voted: false},
    { id: 6, title: 'Python. Wydanie IV', author: 'Mark Lutz', isbn: '978-8324626946', publisher: 'Helion', likes: Math.floor((Math.random() * 10) + 1), voted: false}
  ];


  onSelect(book: Book): void {
    this.selectedBook = book;
  }

  getBooks() : Book[]
  {
    return this.books;
  }
}
